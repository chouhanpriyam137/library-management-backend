const express = require('express');
const router = express.Router();
const User = require('../models/userregister'); 
const { encryptPassword, comparePasswords, generateJwt } = require('../utils/loginutils');

// User Register API
router.post('/userregister', async (req, res) => {
  try {

    const userEmailCheck =
      await User.findOne(
        { email: new RegExp(`^${req.body.email}$`, 'i') }).exec();

    if (userEmailCheck)
      throw new Error('Email already registered');

    req.body.password = await encryptPassword(req.body.password);

    let user = await new User(req.body).save();
    res.status(200).json({ message: "User Register Successfully", data: user, success: true });

  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });
    else
      res.json({ message: 'Error', data: err, success: false });
  }
});

// User Login API
router.post('/userlogin', async (req, res) => {
  try {
    const userlogin = await User.findOne({ email: new 
    RegExp(`^${req.body.email}$`,'i')
   }).exec();

    if (!userlogin) {
      throw new Error('You are not registered');
    }

    const checkPassword = await comparePasswords(req.body.password, userlogin.password);
    if (!checkPassword) 
      throw new Error('Check Your Credentials');
    

    const token = await generateJwt(User._id);
    res.json({ message: 'Logged In', data: token, success: true });
  } catch (err) {
    console.error(err);
    if (err.message)
    res.json({message:err.message,success:false});
  else
    res.json({message:"Error",success:false});
    
  }
});

module.exports = router;




