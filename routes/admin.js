var express = require('express');
var router = express.Router();
var Book=require('../models/book');
var Admin=require('../models/adminregister');


let{encryptPassword,comparePasswords,generateJwt}=require('../utils/loginutils');



/* GET users listing. */
router.get('/login', function(req, res, next) {
  res.send('calling from admin');
});




router.post('/addbook',async(req,res)=>{
  try{
    let book=await new Book(req.body).save();
    res.json({message: "Book added successfully...",
 data: book,success:true})
  }
  catch(err){
    res.json({message:err.message,success:false})
  }
});

router.get('/booklist',async(req,res)=>{
  try{
    const listbook=await Book.find().exec();
    res.json({message: "detail book listing",
 data: listbook,success:true});
  }
  catch(err){
    res.json({message:err.message,success:false})
  }
});

router.delete('/deletebook/:id', async (req, res) => {
  try {
    const bookId = req.params.id;
 
    // Find the product by ID and remove it from the database
    const deletedBook = await Book.findByIdAndRemove(bookId).exec();
 
    if (!deletedBook) {
      // If the product is not found, return an error response
      return res.status(404).json({ message: 'Book not found', success: false });
    }
 
    res.json({ message: 'Book successfully deleted', success: true });
  } catch (err) {
    // Handle any errors during the deletion process
    res.status(500).json({ message: err.message, success: false });
  }
});

router.post('/updatebook/:id', async (req, res) => {
  try {
    const bookId = req.params.id;
    const bookData = req.body; // Assuming you send the updated data in the request body
 
    // Find the product by ID and update it with the new data
    const updatedBook = await Book.findByIdAndUpdate(bookId, bookData, {
      new: true, // To return the updated product instead of the old one
    }).exec();
 
    if (!updatedBook) {
      // If the product is not found, return an error response
      return res.status(404).json({ message: 'Book not found', success: false });
    }
 
    res.json({ message: 'Book successfully updated', book: updatedBook, success: true });
  } catch (err) {
    // Handle any errors during the update process
    res.status(500).json({ message: err.message, success: false });
  }
});
 
router.get('/book/:id', async (req, res) => {
  try {
    const bookId = req.params.id;
 
    // Find the product in the database based on the provided ID
    const book = await Book.findById(bookId);
 
    if (!book) {
      // If the product with the given ID is not found, return an error response
      return res.status(404).json({ message: 'Book not found', success: false });
    }
 
    // If the product is found, return the product details
    res.json({ message: 'Book details fetched successfully', data: book, success: true });
  } catch (err) {
    // Handle any errors that occur during the process
    res.status(500).json({ message: err.message, success: false });
  }
});
 
 

// User Register API starts here 
 
router.post('/adminregister', async (req, res) => {
  try {
 
    const AdminEmailCheck =
      await Admin.findOne(
        { email: new RegExp(`^${req.body.email}$`, 'i') }).exec();
 
    // console.log(adminEmailChk);
    if (AdminEmailCheck)
      throw new Error('Email already registered');
 
    req.body.password = await encryptPassword(req.body.password);
 
    let admin = await new Admin(req.body).save();
    res.status(200).json({ message: "Admin Register Successfully", data: admin, success: true });

 
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });
    else
      res.json({ message: 'Error', data: err, success: false });
  }
})
 
//   admin Register API Close
 
 
// admin Login API Starts Here
 
router.post('/adminlogin', async (req, res) => {
  try {
 
    const adminlogin =
      await Admin.findOne
        ({
          email: new
            RegExp(`^${req.body.email}$`, 'i')
        }).exec();
 
 
    if (!adminlogin)
      throw new Error("You are not registered");
 
    const checkPassword = await
      comparePasswords(req.body.password, adminlogin.password);
 
    if (!checkPassword)
      throw new Error("Check Your Credentials");
 
    const token = await generateJwt(Admin._id);
    res.json({ message: 'Logged In', data: token, success: true });
 
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message,  success: false });
    else
      res.json({ message: 'Error',  success: false });
  }
});


module.exports = router;